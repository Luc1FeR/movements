﻿using Movements;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static System.Single;

namespace UI
{
    public class UISpiral : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _stepField;
        [SerializeField] private TMP_InputField _speedField;
        [SerializeField] private TMP_InputField _loopsField;
        [SerializeField] private Toggle _switchToggle;
        
        [SerializeField] private GameObject _errorWindow;
        [SerializeField] private MoveSpiral _moveSpiral;

        public void TryStartMove()
        {
            if (DataEntered())
            {
                var speed = Parse(_speedField.text);
                var step = Parse(_stepField.text);
                var loops = int.Parse(_loopsField.text);
                
                _moveSpiral.StartMove(speed, step, loops, _switchToggle.isOn);
            }
        }
        
        private bool DataEntered()
        {
            if (_stepField.text.Length == 0 || _speedField.text.Length == 0
                                            || _loopsField.text.Length == 0)
            {
                _errorWindow.SetActive(true);
                return false;
            }

            return true;
        }
    }
}
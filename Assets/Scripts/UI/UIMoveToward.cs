﻿using Movements;
using TMPro;
using UnityEngine;
using static System.Single;

namespace UI
{
    public class UIMoveToward : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _startInputX;
        [SerializeField] private TMP_InputField _startInputY;
        [SerializeField] private TMP_InputField _finishInputX;
        [SerializeField] private TMP_InputField _finishInputY;
        [SerializeField] private TMP_InputField _speedInput;
        
        [SerializeField] private GameObject _errorWindow;
        [SerializeField] private MoveToward _moveToward;

        public void TryStartMove()
        {
            if (DataEntered())
            {
                var startX = Parse(_startInputX.text);
                var startY = Parse(_startInputY.text);
                
                var finishX = Parse(_finishInputX.text);
                var finishY = Parse(_finishInputY.text);

                Vector3 startPosition = new Vector3(startX, 0, startY);
                Vector3 finishPosition = new Vector3(finishX, 0, finishY);

                var speed = Parse(_speedInput.text);
                
                _moveToward.StartMove(startPosition, finishPosition, speed);
            }
        }
        
        private bool DataEntered()
        {
            if (_startInputX.text.Length == 0 || _startInputY.text.Length == 0 || 
                _finishInputX.text.Length == 0 || _finishInputY.text.Length == 0 ||
                _speedInput.text.Length == 0)
            {
                _errorWindow.SetActive(true);
                return false;
            }

            return true;
        }
    }
}
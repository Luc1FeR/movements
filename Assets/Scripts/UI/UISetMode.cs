﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class UISetMode : MonoBehaviour
    {
        private const string Spiral = "Spiral";
        private const string Toward = "MoveToward";
        
        public void SetSpiralType()
        {
            SceneManager.LoadScene(Spiral);
        }

        public void SetTowardType()
        {
            SceneManager.LoadScene(Toward);
        }
    }
}
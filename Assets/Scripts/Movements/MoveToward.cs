﻿using System.Collections;
using UnityEngine;

namespace Movements
{
    public class MoveToward : MonoBehaviour
    {
        [SerializeField] private Vector3 _startPosition;
        [SerializeField] private Vector3 _finishPosition;
        [SerializeField] private float _speed;

        public void StartMove(Vector3 startPosition, Vector3 finishPosition, float speed)
        {
            _startPosition = startPosition;
            _finishPosition = finishPosition;
            _speed = speed;
            transform.position = _startPosition;
            StartCoroutine(Move());
        }

        private IEnumerator Move()
        {
            float progress = 0;
            while (true)
            {
                progress += _speed * Time.deltaTime;
                transform.position = Vector3.Lerp(_startPosition, _finishPosition, progress);
                if (progress > 1) yield break;
                yield return null;
            }
        }
    }
}
﻿using System.Collections;
using UnityEngine;
using static UnityEngine.Mathf;

namespace Movements
{
    public class MoveSpiral : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _step;
        [SerializeField] private int _numLoops;
        [SerializeField] private bool _inMiddle;
        private float _t;


        public void StartMove(float speed, float step, int numLoops, bool inMiddle)
        {
            _speed = speed;
            _step = step;
            _numLoops = numLoops;
            _inMiddle = inMiddle;
            _t = _inMiddle ? 1 : 0;
            StartCoroutine(_inMiddle ? MoveInMiddle() : MoveOutside());
        }

        private IEnumerator MoveInMiddle()
        {
            while (_t > 0)
            {
                CalculateMove();
                _t -= Time.deltaTime * _speed;
                yield return new WaitForFixedUpdate();
            }
        }

        private IEnumerator MoveOutside()
        {
            while (_t < 1)
            {
                CalculateMove();
                _t += Time.deltaTime * _speed;
                yield return new WaitForFixedUpdate();
            }
        }

        private void CalculateMove()
        {
            var radius = Lerp(0, _numLoops * _step, _t);
            var angle = Lerp(0, 2 * PI * _numLoops, _t);
            var onCircle = new Vector3(Sin(angle), 0f, Cos(angle));
            transform.position = onCircle * radius;
        }
    }
}